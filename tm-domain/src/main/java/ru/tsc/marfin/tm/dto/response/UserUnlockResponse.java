package ru.tsc.marfin.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserUnlockResponse extends AbstractResponse {
}
