package ru.tsc.marfin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class DataXmlJaxbLoadRequest extends AbstractUserRequest {

    public DataXmlJaxbLoadRequest(@Nullable final String token) {
        super(token);
    }

}
