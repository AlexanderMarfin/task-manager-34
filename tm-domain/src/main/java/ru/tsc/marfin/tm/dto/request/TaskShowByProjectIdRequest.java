package ru.tsc.marfin.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskShowByProjectIdRequest(@Nullable final String token, @Nullable final String projectId) {
        super(token);
        this.projectId = projectId;
    }

}
