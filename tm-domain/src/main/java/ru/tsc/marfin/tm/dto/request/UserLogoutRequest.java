package ru.tsc.marfin.tm.dto.request;

import org.jetbrains.annotations.Nullable;

public final class UserLogoutRequest extends AbstractUserRequest {

    public UserLogoutRequest(@Nullable final String token) {
        super(token);
    }

}
