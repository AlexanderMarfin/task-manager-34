package ru.tsc.marfin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.marfin.tm.api.service.IAuthService;
import ru.tsc.marfin.tm.api.service.IServiceLocator;
import ru.tsc.marfin.tm.dto.request.UserLoginRequest;
import ru.tsc.marfin.tm.dto.request.UserLogoutRequest;
import ru.tsc.marfin.tm.dto.request.UserShowProfileRequest;
import ru.tsc.marfin.tm.dto.response.UserLoginResponse;
import ru.tsc.marfin.tm.dto.response.UserLogoutResponse;
import ru.tsc.marfin.tm.dto.response.UserShowProfileResponse;
import ru.tsc.marfin.tm.model.Session;
import ru.tsc.marfin.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.marfin.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) {
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        check(request);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserShowProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserShowProfileRequest request
    ) {
        @Nullable final Session session = check(request);
        @Nullable final User user = getServiceLocator().getUserService().findOneById(session.getUserId());
        return new UserShowProfileResponse(user);
    }

}
