package ru.tsc.marfin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description);

}
